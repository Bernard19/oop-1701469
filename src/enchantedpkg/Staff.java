package enchantedpkg;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.text.ParseException;

public class Staff extends User{
	private String name;
	private Date date_birth;
	private Date dateEmployed;
	boolean avail;
	private static RoomDB roomDB;
	
	public Staff() {
		super(0, "");
		this.name = "";
		this.date_birth = new Date();
		this.dateEmployed = new Date();
		this.avail =true;
	}
	
	public Staff(int id, String pas, String na, Date d, Date emp) {
		super(id, pas);
		this.name = na;
		this.date_birth = d;
		this.dateEmployed = emp;
		this.avail =true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date_birth;
	}

	public void setDate(Date date) {
		this.date_birth = date;
	}

	public Date getDateEmployed() {
		return dateEmployed;
	}

	public void setDateEmployed(Date dateEmployed) {
		this.dateEmployed = dateEmployed;
	}
	
	@Override
	public void Login() {
		int i = 0;
		String pas = "";
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter your id\n");
		i = in.nextInt();
		
		System.out.println("Please enter your password\n");
		pas = in.next();
		
		boolean cont = true;
		
		try{
			FileInputStream dFile = new FileInputStream("staff.txt");
			try{
				ObjectInputStream input = new ObjectInputStream(dFile);
				while(cont) {
		    		  try{
		    			  Staff obj = (Staff) input.readObject();
		    			  
		    			  if(obj != null && obj.getId() == i && obj.getPassword() == pas) {
				    		  //objOut.close();
				    		  //locFile.close();
				    		  cont = false;
				    		  
				    		  System.out.println("Access granted");
				    		  Driver.menu();
				      }
		    		  }
		    		  catch(ClassNotFoundException e) {
		    			  e.getStackTrace();
		    		  }
		    		  
				      
		    	  }
				input.close();
			}
			catch(IOException e) {
				e.getStackTrace();
			}
		      
	    	  
		}
		catch(FileNotFoundException e) {
			e.getStackTrace();
		}
		in.close();
		
	}
	
	public boolean isAvail() {
		return avail;
	}

	public void setAvail(boolean avail) {
		this.avail = avail;
	}



	class GetStaffInput{

	    public void main(String[] args){

	    	String name;  //the data that will be entered by the staff member
	        BufferedReader reader; //an instance of the BufferedReader class //will be used to read the data
	        reader = new BufferedReader(new InputStreamReader(System.in)); //specify the reader variable  //to be a standard input buffer

	        System.out.println("What is your name? "); //ask the user for their name

	        try{

	            //read the data entered by the user using 
	            //the readLine() method of the BufferedReader class
	            //and store the value in the name variable
	            name = reader.readLine(); 

	            //print the data entered by the user
	            System.out.println("Your name is " + name);
	        
	          }catch (IOException ioe){

	            //statement to execute if an input/output exception occurs
	            System.out.println("An unexpected error occured.");
	        
	        }
	    } // closes main for Staff 
	} // closes Get StaffInput
	        
	class Dates{
		private Object a;

		public void main(String arrg[]) throws ParseException {
			String dateString = "01/08/1985";
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); // formatter = new SimpleDateFormat("dd/MM/yyyy");
		    a = null;
			((Staff) a).setDate(formatter.parse(dateString));
			
			System.out.println ("Your date of birth is " + dateString);
			
			Date dateEmployed = new Date(); 
			
			System.out.println("The date you were employed is " + dateEmployed); 
		}	
	}  // closes Date class
	    
   class RoomDB 
	{
	    private ArrayList<Room> room;
	    private String[] roomID = {"Room1","Room2"};

	    RoomDB(){

	        room = new ArrayList<Room>();                     
	        for(int i=0;i<roomID.length;i++)
	        {
	            addRoom(new Room(i, roomID[i], i, i, null));
	        }

	    }

	    public void addRoom(Room addRoom){
	        room.add(addRoom);
	    }

	    public ArrayList<Room> getRoom(){
	        return room;
	    }
	}
	
	public class Booking {

	    RoomDB roomDB = new RoomDB();

	    public void main(String[] args) {
	        String roomID;      
	        Object room;  
	        Scanner inputID = new Scanner(System.in);

	        while(true){

	            System.out.println("Please choose the room you want to book \n"+getRoomList(roomDB));

	            while(true){
	                System.out.println("Enter your Room no. : (Enter x for quite ) : ");
	                roomID = inputID.nextLine();
	                System.out.println("X : " + roomID.equals("x"));
	                if(roomID.equals("x")){
	                    System.out.println("Break");
	                    break;
	                }

	                if(getRoomList(roomID) == null){

	                    System.out.println("The room ID is incorrect, please enter again or enter x to quit");

	                }
	                else{

	                    room = getRoomList(roomID);

	                    if(!((Room) room).isBooked()){
	                        System.out.println("Book successfully");
	                        setBooked(true);
	                        break;
	                    }
	                    else{
	                        System.out.println("Please enter the room ID again or enter x to quit");
	                    }
	                    inputID.close();  
	                }

	            }

	        }  
	    }
	}

	public String getRoomList(RoomDB roomDB) {
		return null;
	}

	public void setDate(java.util.Date parse) {
		// TODO Auto-generated method stub
		
	}

	public void setDateofBrith() {
		// TODO Auto-generated method stub
		
	}

	public void setBooked(boolean b) {
	}

	public Object getRoomList(String roomID) {
		return null;
	}


 public static Room getRoom(String roomID){

        roomDB = null;
		for(Room r:roomDB.getRoom()){
            if(r.getRoomID().equals(roomID)){
                System.out.println("r.getRoomID : " + r.getRoomID() + "  AND user.roomID :" + roomID);
                return r;
            }
        }
        return null;
  }
}
 
 
