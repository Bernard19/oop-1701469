package enchantedpkg;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;
import java.io.*;

public class Customer extends User{
	private String name;
	private int number;
	private String email;
	private Date dob;
	
	
	public Customer(int id, String pas, String n, int num, String m, Date d) {
		super(id, pas);
		this.name = n;
		this.number = num;
		this.email = m;
		this.dob = d;
	}
	public Customer() {
		super(0, "");
		this.name = "";
		this.number = 0;
		this.email = "";
		this.dob = new Date();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	@Override
	public void Login() {
		int i = 0;
		String pas = "";
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter your id\n");
		i = in.nextInt();
		
		System.out.println("Please enter your password\n");
		pas = in.next();
		
		boolean cont = true;
		
		try{
			FileInputStream dFile = new FileInputStream("Customer.txt");
			try{
				ObjectInputStream input = new ObjectInputStream(dFile);
				while(cont) {
		    		  try{
		    			  Customer obj = (Customer) input.readObject();
		    			  
		    			  if(obj != null && obj.getId() == i && obj.getPassword() == pas) {
				    		  //objOut.close();
				    		  //locFile.close();
				    		  cont = false;
				    		  
				    		  System.out.println("Access granted");
				    		  Driver.menu();
				      }
		    		  }
		    		  catch(ClassNotFoundException e) {
		    			  e.getStackTrace();
		    		  }
		    		  
				      
		    	  }
				input.close();
			}
			catch(IOException e) {
				e.getStackTrace();
			}
		      
	    	  
		}
		catch(FileNotFoundException e) {
			e.getStackTrace();
		}
		in.close();
		
	}
	
	
	public void ViewRooms() {
		try {
			Scanner read = new Scanner(new File("Rooms.dat"));
			while(read.hasNext()) {
				System.out.println("The Room number is:-"+ read.nextInt()+"\n"+"The Room Type is:-"+read.next()+"\n"+"Number of beds:-"+read.nextInt()+"\n"+"Room cost is:-"+read.nextFloat()+"\n"+"Room Deminsion:-"+read.next());
			}
				read.close();
			}catch (IOException e) {
	    		e.printStackTrace();
	    	}
	}
	
	
	public void SearchRooms() {
	        Scanner read = new Scanner(new File("Rooms.dat"));
			int number = 0;
			String search ="";
			String rooms="";
			
			for(int i=0; i<Room.getRoomNumber();i++)
            {
              String search_room=Room.getRoomNumber(i);

               String [] sroom= search_room.split(" ");

               if(sroom.equals(sroom[0]) || search.equals(sroom[1]))
                  {
            	   	  number++;
                      rooms=rooms+"\n"+Room.getRoomNumber(i).toString();

                  }
               System.out.println(number + " result(s) found for "+ search);
               System.out.println("\n "+ rooms);
            }
	}
	
		public void MakeReservations(int resId, int roomNumber, Date reservationDate){
			
			int id;
			int roomnum;
			Date resdate;
			
			ViewRooms();
	
			try {	
			Scanner readres= new Scanner(new File("Reservation.dat"));
			while(readres.hasNext()) {
				System.out.println("Below are existing reservations");
				System.out.println("The Reservation Id is:-"+readres.nextInt()+"\t"+"The Room Number is:-"+readres.nextInt()+"\t"+"The Resevation Date is:-"+readres.next());
			}
			readres.close();
			}catch (IOException e) {
				e.printStackTrace();
			}
			Scanner store = new Scanner(System.in);
			Date date = new Date();
			System.out.println("Enter Reservation Id\t:");
			id = store.nextInt();
			System.out.println("Enter Room Number");
			roomnum = store.nextInt();
			System.out.println("Enter Reservation Date");
			System.out.println("Please Enter day");
			date.setDay(store.nextInt());
			System.out.println("Please Enter month");
			date.setMonth(store.nextInt());
			System.out.println("Please Enter year");
			date.setYear(store.nextInt());
			resdate = date;
			store.close();
			Reservation MakeReservation = new Reservation(id, roomnum, resdate);
			
				  try (DataOutputStream resfile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("Reservation.dat")))){
	    				ObjectOutputStream objOut = new ObjectOutputStream(resfile);
	    				objOut.writeObject(MakeReservation);
	    				objOut.close();
	    				
	    				resfile.close();
	    			}
	    			catch(IOException e) {
	    				System.out.println("In catch block");
	    				e.printStackTrace();
	    			}
			}
			
			
			
			
			
			
			
			
			}	

