package enchantedpkg;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ParseException extends Exception {
	public class ParseExceptionExample {
		 
	    public void main(String[] args) throws ParseException {
	        String dateStr = "1980-11-19";
	        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	         
	            java.util.Date date = null;
	             
	            try {
					date = dateFormat.parse(dateStr);
				} catch (java.text.ParseException e) {
					e.printStackTrace();
				}
				System.out.println(date);
	    }
	}
}
