package enchantedpkg;

public abstract class User {
	private int id;
	private String password;
	
	public User() {
		this.id = 0;
		this.password = "";
	}
	
	public User(int id, String pas) {
		this.id = id;
		this.password = pas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public abstract void Login();
	
	
}

