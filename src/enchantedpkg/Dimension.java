package enchantedpkg;

public class Dimension {
	private int width;
	private int length;
	
	public Dimension() {
		this.width = 0;
		this.length = 0;
	}
	
	public Dimension(int wid, int len) {
		this.width = wid;
		this.length = len;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	
}
