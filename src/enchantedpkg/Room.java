package enchantedpkg;

public class Room {
	private int roomNumber;
	private String roomType;
	private int numOfBed;
	private float cost;
	private Dimension dimension;
	
	public Room() {
		this.roomNumber = 0;
		this.roomType = "";
		this.numOfBed = 0;
		this.cost = 0;
		this.dimension = new Dimension();
	}
	
	public Room(int num, String type, int bed, float cost, Dimension dim) {
		this.roomNumber = num;
		this.roomType = type;
		this.numOfBed = bed;
		this.cost = cost;
		this.dimension = dim;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public int getNumOfBed() {
		return numOfBed;
	}

	public void setNumOfBed(int numOfBed) {
		this.numOfBed = numOfBed;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public Dimension getDimension() {
		return dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public boolean isBooked() {
		return false;
	}

	public Object getRoomID() {
		return null;
	}	
}
