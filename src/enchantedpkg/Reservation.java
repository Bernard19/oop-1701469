package enchantedpkg;

public class Reservation {
	private int resId;
	private int roomNumber;
	private Date reservationDate;
	
	public Reservation() {
		this.resId = 0;
		this.roomNumber = 0;
		this.reservationDate = new Date();
	}
	
	public Reservation(int id, int room, Date res) {
		this.resId = id;
		this.roomNumber = room;
		this.reservationDate = res;
	}

	public int getResId() {
		return resId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}
	
	
}
