package enchantedpkg;
import java.util.*;
import java.io.*;

public class Admin extends User{
	
	// this is using overloading concept and calling a different instance of admin within admin.
//	public Admin() {
//			this(0, "");
//	}
	static final String adminFILEPATH = "admin.dat";
	static final String staffFILEPATH = "staff.dat";
	static final String cusFILEPATH = "customer.dat";
	
	
		
	public Admin() {
		super(0, "");
	}
	
	public Admin(int id, String pas) {
		super(id, pas);
	}
	
	

	@Override
	public void Login() {
		
		int i = 0;
		String pas = "";
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter your id\n");
		i = in.nextInt();
		
		System.out.println("Please enter your password\n");
		pas = in.next();
		
		boolean cont = true;
		
		try{
			FileInputStream dFile = new FileInputStream("admin.txt");
			try{
				ObjectInputStream input = new ObjectInputStream(dFile);
				while(cont) {
					System.out.println("In loop\n");
		    		  try{
		    			  Admin obj = (Admin) input.readObject();
		    			  System.out.println(obj.getId() + "\n");
		    			  System.out.println(obj.getPassword() + "\n");
		    			  
		    			  if(obj != null && obj.getId() == i && obj.getPassword() == pas) {
				    		  //objOut.close();
				    		  //locFile.close();
				    		  cont = false;
				    		  
				    		  System.out.println("Access granted\n");
				    		  Driver.menu();
				      }
		    		  }
		    		  catch(ClassNotFoundException e) {
		    			  e.getStackTrace();
		    		  }
		    		  
				      
		    	  }
				input.close();
			}
			catch(IOException e) {
				e.getStackTrace();
			}
		      
	    	  
		}
		catch(FileNotFoundException e) {
			e.getStackTrace();
		}
		in.close();
		
		    	  
		
	}

	public void addUser() throws IOException{
		Scanner inn = new Scanner(System.in);
		String user = "";
		String pass = "";
		int i = 0;
		
		System.out.println("What user are you adding to the system?" + "\n");
		System.out.println("Please enter (a) for admin, (s) and (c) for customer" + "\n");
		user = inn.next();
		
		if(user.toLowerCase().equals("a")) {
			
			Scanner in = new Scanner(System.in);
			System.out.println("Please Enter new admins id\n");
			i = in.nextInt();
			System.out.println("Please Enter new admins password\n");
			pass = in.next();
			
			Admin a = new Admin(i, pass);
			
			addUser(a, adminFILEPATH);
			in.close();
		} else if(user.toLowerCase() == "s") {
			String name = "";
			Date date = new Date();
			Date dateEmployed = new Date();
			
			Scanner in = new Scanner(System.in);
			System.out.println("Please Enter new staff id");
			i = in.nextInt();
			System.out.println("Please Enter new staff password");
			pass = in.next();
			System.out.println("Please Enter new staff name");
			name = in.nextLine();
			System.out.println("Please Enter year of birth." + "\n");
			//date.of(in.nextInt(), in.nextInt(), in.nextInt());
			
			
			System.out.println("Please Enter day of birth");
			date.setDay(in.nextInt());
			System.out.println("Please Enter month of birth");
			date.setMonth(in.nextInt());
			System.out.println("Please Enter year of birth");
			date.setYear(in.nextInt());
			
			System.out.println("Please Enter new staff employment date");
			//dateEmployed = in;
			System.out.println("Please Enter day of employment");
			dateEmployed.setDay(in.nextInt());
			System.out.println("Please Enter month of employment");
			dateEmployed.setMonth(in.nextInt());
			System.out.println("Please Enter year of employment");
			dateEmployed.setYear(in.nextInt());
			
			Staff stf = new Staff(i, pass, name, date, dateEmployed);
			addUser(stf, staffFILEPATH);
			in.close();
		} else if(user.toLowerCase() == "c") {
			String name = "";
			int number = 0;
			String email = "";
			Date dob = new Date();
			
			Scanner in = new Scanner(System.in);
			System.out.println("Please enter new customers id");
			i = in.nextInt();
			System.out.println("Please enter new customers password");
			pass = in.next();
			System.out.println("Please enter new customers name");
			name = in.nextLine();
			System.out.println("Please enter new customers contact number");
			number = in.nextInt();
			System.out.println("Please enter new customers email");
			email = in.nextLine();
			System.out.println("Please enter new customers date of birth");
			System.out.println("Please enter day of birth");
			dob.setDay(in.nextInt());
			System.out.println("Please enter month of birth");
			dob.setMonth(in.nextInt());
			System.out.println("Please enter year of birth");
			dob.setYear(in.nextInt());
			
			Customer cus = new Customer(i, pass, name, number, email, dob);
			addUser(cus, cusFILEPATH);
			in.close();
			/////////////////////////////////////////////
		}
		
		inn.close();
	}
	
	public void addUser(User u, String path) throws IOException{
		
		try (DataOutputStream locFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path)))){
			ObjectOutputStream objOut = new ObjectOutputStream(locFile);
			objOut.writeObject(u);
			objOut.close();
			
			locFile.close();
		}
		catch(IOException e) {
			System.out.println("In catch block");
			e.printStackTrace();
		}
	}

	
	
	public void deleteUser() {
		Scanner in = new Scanner(System.in);
		int id = 0;
		String option = "";
		//String type = "";
		FileInputStream dFile = null;
		String FILEPATH = "file.dat";
		
		System.out.println("Which type of user do you wish to delete. Enter (a) for admin, (s) for staff and (c) for customer\n");
		option = in.next();
		
		if(option == "a") {
			FILEPATH = "admin.dat";
			//ArrayList<Admin> objectsList = new ArrayList<Admin>();
			try{
				dFile = new FileInputStream(FILEPATH);
			}
			catch(FileNotFoundException e) {
				e.getStackTrace();
			}
		}
		else if(option == "s") {
			FILEPATH = "staff.dat";
			//ArrayList<Staff> objectsList = new ArrayList<Staff>();
			try{
				dFile = new FileInputStream(FILEPATH);
			}
			catch(FileNotFoundException e) {
				e.getStackTrace();
			}
		}
		else if(option == "c") {
			FILEPATH = "customer.dat";
			//ArrayList<Customer> objectsList = new ArrayList<Customer>();
			try{
				dFile = new FileInputStream(FILEPATH);
			}
			catch(FileNotFoundException e) {
				e.getStackTrace();
			}
		}
		
		System.out.println("Please enter the id of user to be deleted\n");
		id = in.nextInt();
		
		
		boolean cont = true;
		
		try{
		   ObjectInputStream input = new ObjectInputStream(dFile);
		      
		      if(FILEPATH == "admin.dat") {
		    	  while(cont) {
		    		  Admin obj = (Admin) input.readObject();
		    		  
		    		  if(obj != null) {
				    	  if(obj.getId() == id) {
				    		  System.out.println("User found and deleted\n");
				    	  }
				    	  else {
				    		  try (DataOutputStream locFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("temp.dat")))){
				    				ObjectOutputStream objOut = new ObjectOutputStream(locFile);
				    				objOut.writeObject(obj);
				    				objOut.close();
				    				
				    				locFile.close();
				    			}
				    			catch(IOException e) {
				    				System.out.println("In catch block");
				    				e.printStackTrace();
				    			}
				    	  }
				      }
				      else {
				    	  cont = false;
				    	  File old = new File(FILEPATH);
				    	  File temp = new File("temp.dat");
				    	  temp.renameTo(old);
				    	  old.delete();
				      }/////////////////////////////////////////////////////////////////
		    	  }
		    	  
				}
				else if(FILEPATH == "staff.dat") {
					while(cont) {
					Staff obj = (Staff) input.readObject();
					
					if(obj != null) {
				    	  if(obj.getId() == id) {
				    		  System.out.println("User found and deleted\n");
				    	  }
				    	  else {
				    		  try (DataOutputStream locFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("temp.dat")))){
				    				ObjectOutputStream objOut = new ObjectOutputStream(locFile);
				    				objOut.writeObject(obj);
				    				objOut.close();
				    				
				    				locFile.close();
				    			}
				    			catch(IOException e) {
				    				System.out.println("In catch block");
				    				e.printStackTrace();
				    			}
				    	  }
				      }
				      else {
				    	  cont = false;
				    	  File old = new File(FILEPATH);
				    	  File temp = new File("temp.dat");
				    	  temp.renameTo(old);
				    	  old.delete();
				      }
				}
				}
				else if(FILEPATH == "customer.dat") {
					while(cont) {
					Customer obj = (Customer) input.readObject();
					
					if(obj != null) {
				    	  if(obj.getId() == id) {
				    		  System.out.println("User found and deleted\n");
				    	  }
				    	  else {
				    		  try (DataOutputStream locFile = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("temp.dat")))){
				    				ObjectOutputStream objOut = new ObjectOutputStream(locFile);
				    				objOut.writeObject(obj);
				    				objOut.close();
				    				
				    				locFile.close();
				    			}
				    			catch(IOException e) {
				    				System.out.println("In catch block");
				    				e.printStackTrace();
				    			}
				    	  }
				      }
				      else {
				    	  cont = false;
				    	  File old = new File(FILEPATH);
				    	  File temp = new File("temp.dat");
				    	  temp.renameTo(old);
				    	  old.delete();
				    	  //Driver.menu();
				      }
				}
				}
		      
		}catch(Exception e){
		   //System.out.println(e.printStackTrace());
		}
		in.close();
		
		//ObjectInputStream objOut = new ObjectInputStream(locFile);
		
		
	}
	
	
}
