package enchantedpkg;

import java.util.*;

public class Driver {

	static final String cusFILEPATH = "customer";
	
	public static void main(String[] args) {
		
		int opt =0;
		Scanner in = new Scanner(System.in);
		
		System.out.println("\n::==============================================================::\n");
        System.out.println("::\t          Welcome to Enchanted Hotel                 \t::");
        System.out.println("\n::==============================================================::\n");
        
        System.out.println("\n::Before Entering the system please indicate which type of user are you::\n");
        System.out.println("::[1] Admin \t\t\t\t\t\t\t::\n");
        System.out.println("::[2] Staff \t\t\t\t\t\t\t::\n");
        System.out.println("::[3] Customer \t\t\t\t::\n");
        opt = in.nextInt();
        
        switch(opt)
        {
        	case 1:
        		Admin ad = new Admin();
//        		try{
//        			ad.addUser();
//        		}
//        		catch(IOException e) {
//        			e.getStackTrace();
//        		}
        		ad.Login();
                break;
            case 2:
            	Staff sta = new Staff();
            	sta.Login();
                break;
            case 3:
            	Customer cus = new Customer();
            	cus.Login();
                break;
         
            default:
            	System.out.println("Invalid selection, try again\n");
                
        }
        
        in.close();
        

	}
	
	public static void menu(){
		int choice=0;

	    while(choice !=4)
	    {
	    	Scanner in = new Scanner(System.in);
	    	
	    	System.out.println("\n::==============================================================::\n");
	        System.out.println("::\t          Welcome to Enchanted Hotel                 \t::");
	        System.out.println("\n::==============================================================::\n");
	        System.out.println("::Please select an option from the following menu to perform:\t::\n");
	        System.out.println("::[1] Make Reservation\t\t\t\t\t\t\t::\n");
	        System.out.println("::[2] Customer Check-out\t\t\t\t\t\t\t::\n");
	        System.out.println("::[3] Add Users\t\t\t\t::\n");
	        System.out.println("::[4] Delete Users\t\t\t\t\t\t\t::\n");
	        System.out.println("::[5] Make Reservation\t\t\t\t\t\t\t::\n");
	        System.out.println("::[6] Add Room\t\t\t\t\t\t\t::\n");
	        System.out.println("::[7] Delete Room\t\t\t\t\t\t\t::\n");
	        System.out.println("::[8] View Rooms\t\t\t\t\t\t\t::\n");
	        System.out.println("::[9] Edit Room\t\t\t\t\t\t\t::\n");
	        System.out.println("::[10] Search Room\t\t\t\t\t\t\t::\n");
	        System.out.println("::[0] Exit\t\t\t\t\t\t\t::\n");
	        System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n");
	        choice = in.nextInt();

	        switch(choice)
	        {
	        	case 1:
	        		
	                break;
	            case 2:
	            	
	                break;
	            case 3:
	            	
	                break;
	            case 4:
	                System.out.println("Thank you for using the program ...........:");
	              
	                break;
	            default:
	            	System.out.println("Invalid selection, try again\n");
	                
	        }
	        in.close();
	    }
		
		/*System.out.System.out.println("\t\tWelcome to Enchanted Hotel \n\n");
		System.out.System.out.println("Please select an option from the following menu to perform \n");
		System.out.System.out.println("1 :Make Reservation \n");
		System.out.System.out.println("2 :Customer Check-out \n");
		System.out.System.out.println("3 :Total number of rooms occupied \n");
		System.out.System.out.println("4 :Number of Single rooms available \n");
		System.out.System.out.println("5 :Number of Couple rooms available \n");
		System.out.System.out.println("6 :Number of Family rooms available \n");
		System.out.System.out.println("7 :To determine the status of a given room \n");
		System.out.System.out.println("0 :Exit \n");*/
		
//		try{
//			FileInputStream dFile = new FileInputStream(cusFILEPATH);
//		}
//		catch(IOException e) {
//			e.getStackTrace();
//		}
//		System.out.println(dFile);

	}

}
